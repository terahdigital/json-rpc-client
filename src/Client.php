<?php declare(strict_types=1);


namespace Terah\JsonRpcClient;

use Terah\RestClient\RestClient;
use Terah\RestClient\RestClientTrait;

class Client
{
    use RestClientTrait;

    public function __construct(RestClient $restClient)
    {
        $this->setRestClient($restClient);
    }


}
